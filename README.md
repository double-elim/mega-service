## The Mega Service

This bracket management software consists of 2 main parts: accounts and series. Accounts can be either head TOs or volunteer TOs. Series (think Genesis) contain tournaments (Genesis 1, 2, 3, etc.), and tournaments contain events (singles, doubles).

The MegaService's purpose is to handle the creation and execution of the series, tournaments and events.
